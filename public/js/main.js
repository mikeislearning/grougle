$(document).ready(function() {
    //calls the function based on the value of the dropdown list
    getDeals($('#type').val());
    //when the value of the dropdown list changes, run the function again
    $('#type').change(function() {
        //function call with 'this' holding the value of '#type'
        clearOverlays();
        getDeals($(this).val());
    });

});

var map = null,
    grouponUrl = 'https://api.groupon.com/v2/deals.json?client_id=18457beb5ec50497218429e906c7e98a3ba99b21',
    markers = [];

google.maps.event.addDomListener(window, 'load', initializeMap);
google.maps.visualRefresh = true;

//sets up the map
function initializeMap() {

    getLocation();
    //function that is run when the map is initialized
    function getLocation() {
        if (navigator.geolocation) {
            //uses geolocation to find where you are
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            console.log = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        var currentLat = position.coords.latitude,
            currentLng = position.coords.longitude,
            mapProp = {
                //should indicate the value here based on geolocation
                center: new google.maps.LatLng(currentLat, currentLng),
                zoom: 9,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
        map = new google.maps.Map(document.getElementById('googleMap'), mapProp);
        placeMarker(currentLat, currentLng, "You are here");

    }
}

//creates a google maps marker
function placeMarker(lat, lng, content) {

    var center = new google.maps.LatLng(lat, lng),
        infowindow = new google.maps.InfoWindow({
            content: content,
            map: map
        }),
        marker = new google.maps.Marker({
            position: center,
            map: map
        });
    //adds each market to the array
    markers.push(marker);
    infowindow.open(map, marker);

}


function setAllMap(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function clearOverlays() {
    setAllMap(null);
}

//the dtype argument is to set by the value of the dropdown list
function getDeals(dtype) {
    //appends some html to the deals id
    $('#deals').html('');
    //ajax calls the groupon API, where function(data) releases groupon data
    $.ajax({
        url: grouponUrl,
        dataType: 'jsonp',
        success: function(data) {
            //makes sure cross domain requests are valid
            $.support.cors = true;
            //sets an origin, but it doesn't do anything yet
            var origin = "129 Spadina Ave.";
            //sets the initials foreach, running as many times as there are deals
            $.each(data.deals, function(i, item) {
                var dealType = false,
                    destination = "",
                    distance = "";
                //runs a foreach based on each data array index, going through the tags
                $.each(item.tags, function(i, tags) {
                    var type = tags.name;

                    if (type == dtype) {
                        dealType = true;

                    }
                });
                //dealType is set to false, but if true, then this runs
                if (dealType) {
                    //finds the latitude & latitude
                    if (item.options[0].redemptionLocations[0]) {
                        var destinationLat = item.options[0].redemptionLocations[0].lat,
                            destinationLng = item.options[0].redemptionLocations[0].lng,
                            grouponContent = "<a href= '" + item.dealUrl + "'> " + item.title + "</a>";

                        placeMarker(destinationLat, destinationLng, grouponContent);

                        //append the title and the destination to deals
                        $('#deals').append("<li>" + item.announcementTitle + "</li>");
                    }
                }
            });


        }
    });

}
