<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Our Groupon/Google Maps Mashup</title>
		<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="css/main.css" rel="stylesheet" >
	</head>
	<body>
		<div class="container">
			<div class="row-fluid">
				<h1> The Groupon / Noogle Maps Mashup! </h1>
				<h2> Please Select a Deal type to get started </h2>
				<select id="type">
					<option>Select deal type</option>
					<option value="Services">Services</option>
					<option value="Arts and Entertainment">Arts &#038; Entertainment</option>
					<option value="Beauty &#038; Spas">Beauty &#038; Spas</option>
					<option value="Shopping">Shopping</option>
					<option value="Restaurants">Restaurants</option>
				</select>
				<div class="row-fluid">
					<div class="span7">
						<div id="googleMap"></div>
					</div>
					<div class="span5">
						<ol id="deals">
						</ol>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script
		src="http://maps.googleapis.com/maps/api/js?key=AIzaSyB2EY4ZJKs_nxh256EehEOvAerckAVbh-w&sensor=false">
		</script>
		<script src="js/main.js" type="text/javascript"></script>
	</body>
</html>
