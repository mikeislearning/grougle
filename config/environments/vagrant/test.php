<?php if (ENVIRONMENT == 'production') die(); ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<p id="status"></p>

<script>

var page = 0;
//var page = 50;
var key = 'ygrrd5wxhna5rpktsb5r2jm7';

function getApi() {

	var rnd = '';
	for (var i=0; i<10; i++) {
		rnd += '' + parseInt(10 * Math.random());
	}

	page += 1;

	$.post(
		'/api/Restaurants/getrestaurants',
		//'/test.json',
		{
			'data[page]': page,
			'data[rnd]': rnd
		},
		function(response) {
			for (var i=0; i<response.listings.length; i++) {
				var listing = response.listings[i];
				var geoCode = {latitude:0, longitude:0};
				if (listing.geoCode) {
					geoCode = listing.geoCode;
				}
				$.post(
					'/api/restaurants',
					{
						'data[name]': listing.name,
						'data[latitude]': geoCode.latitude,
						'data[longitude]': geoCode.longitude,
						'data[location]': listing.address.city
					},
					function(response) {
						$("#status").append(response.meta.message + "<br>");
					}, 
					"json"
					);
			}

			$("#status").html("Waiting for page " + (page + 1) + "<br>");
			if (page > 50) {
				return;
			}
			setTimeout(getApi, ((60 * Math.random()) + 20) * 1000);
		}
		);
}

getApi();


</script>